(function(){
    'use strict';

    var background_page = chrome.extension.getBackgroundPage();

    function availableStatus()
    {
        $('.gf-status').text(chrome.i18n.getMessage('available_status'));
        $('#loggedin .bottom-text').html(chrome.i18n.getMessage('popup_bottom_text_available'));
        $('.status-block .fa').removeClass('fa-hand-o-down');
        $('.status-block .fa').addClass('fa-hand-o-up green-color');
    }

    function unavailableStatus()
    {
        $('.gf-status').text(chrome.i18n.getMessage('unavailable_status'));
        $('#loggedin .bottom-text').html(chrome.i18n.getMessage('popup_bottom_text_unavailable'));
        $('.status-block .fa').removeClass('fa-hand-o-up green-color');
        $('.status-block .fa').addClass('fa-hand-o-down');
    }

    $('.onoffswitch-checkbox').change(function(){
        if ($('#myonoffswitch').prop('checked') == true) {
            availableStatus();
            background_page.chrome.browserAction.setIcon({path :'../app/img/active.png'});

            background_page.GF.pubnub.subscribe({
                connect: function(m) {
                    background_page.GF.setStatus(true);
                },
                callback: function(m){},
                channel: 'onlineGF',
                error: function(m) {
                    background_page.console.log(m);
                }
            });
        } else {
            unavailableStatus();
            background_page.GF.setStatus(false);
            background_page.chrome.browserAction.setIcon({path :'../app/img/heart.png'});
            background_page.GF.pubnub.unsubscribe({
               channel : 'onlineGF'
            });
        }
    });

    if (background_page.GF.is_logged_in) {
    	$('#loggedin').show();
    	$('#unlogged').hide();

        $('#popup-title').text(chrome.i18n.getMessage('popup_title'));

        if (background_page.GF.is_available) {
            $('.onoffswitch-checkbox').prop('checked', true);
            availableStatus();
        } else {
            unavailableStatus();
        }
    } else {
    	$('#loggedin').hide();
    	$('#unlogged').show();

        $('#popup-title').text(chrome.i18n.getMessage('popup_title'));
        $('.gf-status').text(chrome.i18n.getMessage('logged_out_status'));
        $('#unlogged .bottom-text').html(chrome.i18n.getMessage('popup_bottom_text_logged_out'));
    }

    //document.querySelector('button').addEventListener('click', function(){
     //   background_page.createNotification();
	//	background_page.audioNotification();
		//background_page.popup();
   // });
}())

function handleButtonClick()
{
    chrome.tabs.create({
        index: 0,
        url: 'http://google.com'
    }, function(tab){
        console.log(tab);
    });
}

function audioNotification(){
    var yourSound = new Audio('sound.mp3');
    yourSound.play();
}

function createNotification(m){
    var opt = {type: "basic",title: "Your Title",message: "Your message",iconUrl: "../app/img/heart.png"}
    chrome.notifications.create("notificationName",opt,function(){});
}

var pubnub = PUBNUB.init({
    'publish_key': 'pub-c-bd645d1e-f4aa-4719-9008-d14e29514bab',
    'subscribe_key': 'sub-c-8e1b190a-b033-11e4-83d7-0619f8945a4f'
});

var GF = {
    setLoggedIn: function(logged_in){
        this.is_logged_in = logged_in;
    },
    setGFName: function(gf_name) {
        this.gf_name = gf_name;
    },
    setStatus: function(status) {
        this.is_available = status;
    },
    enablePubNub: function(enable) {
        if (enable === true) {

            if (! this.pubnub) {
                this.pubnub = PUBNUB.init({
                    'publish_key': 'pub-c-bd645d1e-f4aa-4719-9008-d14e29514bab',
                    'subscribe_key': 'sub-c-8e1b190a-b033-11e4-83d7-0619f8945a4f',
                    'uuid': md5(this.gf_name)
                });
            }

            var self = this;

            this.pubnub.subscribe({
                callback: function(m){
                    if (self.is_available) {
                        timestamp = new Date().getTime();
                        opt       = {
                            type: "basic",
                            title: "AlphaSSS request from " + m.requestor,
                            message: "A new talk request is pending. You have 5 minutes to accept it.",
                            iconUrl: m.avatar
                        };

                        chrome.notifications.create('id_' + timestamp,opt,function(notificationId){
                            chrome.windows.create({url: chrome.extension.getURL("../app/views/popup/popup-window.html"), type: "panel"}, function(win){
                                console.log(win);
                            });
                        });
                    }
                },
                channel: md5(this.gf_name) + '_session',
                error: function(m) {
                    console.log(m);
                }
            });
        } else {
            this.pubnub = null;
        }
    },
    is_available: false,
    is_logged_in: false,
    gf_name: null,
    pubnub: null
};

function updatePopState(){
    chrome.browserAction.setIcon({path :'../app/img/heart.png'});
    GF.enablePubNub(false);
    chrome.cookies.getAll({}, function(cookies){
        for (i in cookies) {
            if(cookies[i].name.match(/wordpress_logged_in_.*/)) {
                GF.setGFName(cookies[i].value.match(/[^%]*/));
                GF.enablePubNub(true);
            }
        }
    });
}

chrome.cookies.onChanged.addListener(function(changeInfo) {
    if(changeInfo.cookie.name.match(/wordpress_logged_in_.*/)) {
        if (changeInfo.removed == false) {
            GF.setGFName(changeInfo.cookie.value.match(/[^%]*/));
            GF.enablePubNub(true);
        } else {
            GF.setGFName(null);
            chrome.browserAction.setIcon({path :'../app/img/heart.png'});
            GF.enablePubNub(false);
        }
    }
});

// This event Fires when a new User has Left.
pubnub.events.bind( 'presence-user-leave', function(uuid) {
    if (uuid == md5(GF.gf_name)) {
        GF.setLoggedIn(false);
    }
});

// This event Fires when a new User has Left.
pubnub.events.bind( 'presence-user-join', function(uuid) {
    if (uuid == md5(GF.gf_name)) {
        pubnub.state({
           channel: "onlineUsers",
           uuid: uuid,
           callback: function(m){
               // User is GF
               GF.setLoggedIn(m.isGF);
           }
        });
    }
} );

pubnub.subscribe({
    channel: 'onlineUsers',
    callback: function(m) {},
    presence: function(details){
        var uuid = 'uuid' in details && (''+details.uuid).toLowerCase();

        if ('action' in details && uuid) pubnub.events.fire(
            'presence-user-' + details.action, uuid
        );
    }
});

chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse) {

});

chrome.management.onEnabled.addListener(updatePopState);
chrome.runtime.onInstalled.addListener(updatePopState);
